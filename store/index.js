// eslint-disable-next-line prefer-const
let dateObject

export const state = () => ({
  api_key: '88030114c5e47763a011a75e7a10c633',
  url_base: '/forecast/',
  db_log: '/logs',
  query: '',
  weather: {},
  forecasts: [],
  today: ''
})

export const mutations = {
  SET_WEATHER (state, value) {
    state.weather = value
  },
  updateQuery (state, query) {
    state.query = query
  },
  SET_TODAY (state, value) {
    state.today = value
  },
  SET_IP (state, value) {
    state.today = value
  },
  updateLog (state, value) {
    state.log = value
  }
}

export const actions = {
  async setWeather ({ commit }) {
    const { data } = await this.$axios.get(`${this.state.url_base}${this.state.api_key}/${this.state.query}`).catch(() => {
    })
    commit('SET_WEATHER', data)
  },
  async setLog () {
    const datos = this.state.log
    await this.$axios.post(`${this.state.db_log}`, datos)
  },
  // sets todays date in correct format for filters
  setToday ({ commit }) {
    let today = new Date()
    const dd = String(today.getDate()).padStart(2, '0')
    const mm = String(today.getMonth() + 1).padStart(2, '0')
    const yyyy = today.getFullYear()
    today = yyyy + '-' + mm + '-' + dd
    commit('SET_TODAY', today)
  }
}

export const getters = {
  // fiter out remaining results for todays date from 3 hrly data
  threeHourlyToday: (state) => {
    // eslint-disable-next-line prefer-const
    let datas = []
    // eslint-disable-next-line prefer-const
    let forecasts = state.weather.hourly.data
    // eslint-disable-next-line arrow-parens
    forecasts.forEach(element => {
      // eslint-disable-next-line prefer-const
      dateObject = new Date(element.time * 1000)
      // eslint-disable-next-line prefer-const
      element.dates = `${dateObject.getFullYear()}-${String(dateObject.getMonth() + 1).padStart(2, '0')}-${String(dateObject.getDate()).padStart(2, '0')}`
      element.newtime = `${String(dateObject.getHours()).padStart(2, '0')}:${String(dateObject.getMinutes()).padStart(2, '0')}:${String(dateObject.getSeconds()).padStart(2, '0')}`

      if (element.dates === state.today) {
        datas.push(element)
      }
    })
    return datas
  },
  // filter out midday weather from 3hrly data for next 4 days (not today)
  dailyMidday: (state) => {
    // eslint-disable-next-line prefer-const
    let datasMidday = []
    // eslint-disable-next-line prefer-const
    let forecasts = state.weather.daily.data
    // eslint-disable-next-line arrow-parens
    forecasts.forEach(element => {
      // eslint-disable-next-line prefer-const
      dateObject = new Date(parseInt(element.time) * 1000)
      // eslint-disable-next-line prefer-const
      element.dates = `${dateObject.getFullYear()}-${String(dateObject.getMonth() + 1).padStart(2, '0')}-${String(dateObject.getDate()).padStart(2, '0')}`
      element.newtime = `${String(dateObject.getHours()).padStart(2, '0')}:${String(dateObject.getMinutes()).padStart(2, '0')}:${String(dateObject.getSeconds()).padStart(2, '0')}`
      datasMidday.push(element)
    })
    return datasMidday
  }
}
